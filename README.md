# Apollo Server

> Servidor GraphQL no Node.JS HTTP Server, ou em ambientes "sem servidor".
>
> **Typescript** Required - Testing: **v3.5.1**

Demonstração simples no uso do Servidor Apollo GraphQL com suporte em importar os Schemas.

![Demonstração](https://gitlab.com/i.leonardo/init-apollo-server-2/raw/master/main.png)

## Requisitos

> - [apollo-server](https://github.com/apollographql/apollo-server#readme): **v2.6.2**
> - **Plugins GraphQL:**
>   - [graphql-import](https://github.com/graphcool/graphql-import#readme): **v0.7.1** - Importa os arquivos `*.graphql` para criar o schema
> - **Dependências:**
>   - [graphql](https://github.com/graphql/graphql-js): **v14.3.1**
>   - [ts-node-dev](https://github.com/whitecolor/ts-node-dev#readme): **v6.2.0** - Watch `ts-node`
>   - [tsconfig-paths](https://github.com/dividab/tsconfig-paths#readme): **v3.8.0** - Paths directory for `tsconfig.json`
> - **Editor usado:**
>   - Visual Studio Code
>     - [editorconfig.editorconfig](https://marketplace.visualstudio.com/items?itemName=EditorConfig.EditorConfig): **EditorConfig for VS Code**

## Instalação

```bash
# install dependencies
$ npm install

# Start Server Watch in http://localhost:4000 (default)
$ npm run serve

# Start Server production http://localhost:4000 (default)
$ npm start
```

## Árvore (tree)

```
.
|   .editorconfig
|   .gitignore
|   LICENSE
|   package.json
|   README.md
|   tsconfig.json (ts-node)
|
\---src (project)
    |   app.ts (Server)
    |   resolvers.ts (Functions Schema)
    |   schema.graphql (root GraphQL)
    |
    +---Mutations
    +---Queries
    \---Schemas
```

## Licença

> [MIT License](https://gitlab.com/i.leonardo/init-apollo-server-2/blob/master/LICENSE)

Copyright © 2019 [Leonardo C. Carvalho](https://gitlab.com/i.leonardo)
