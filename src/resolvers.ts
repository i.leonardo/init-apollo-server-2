import Hello from '@/Queries/Hello';

export default {
  Query: {
    hello: () => Hello,
  },
};
