import { ApolloServer, gql } from 'apollo-server';
import { importSchema } from 'graphql-import';

import resolvers from '@/resolvers';

const server = new ApolloServer({
  typeDefs: gql(importSchema('src/schema.graphql')),
  resolvers,
  tracing: process.env.NODE_ENV !== 'production',
});

server.listen({
  port: process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 4000
}).then(({ url }) => {
  console.log(`🚀 Server ready at ${url}`);
});
